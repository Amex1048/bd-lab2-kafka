use std::net::TcpStream;

use clap::Parser;
use eyre::Context;
use kafka::producer::{Producer, Record};
use tungstenite::{stream::MaybeTlsStream, WebSocket};

const BITSTAMP_HOST: &str = "wss://ws.bitstamp.net";
const BITSTAMP_EVENT_NAME: &str = "bts:subscribe";
const BITSTAMP_CHANNEL_NAME: &str = "live_orders_btcusd";

#[derive(Debug, Clone, serde::Serialize)]
struct Subscribe {
    event: String,
    data: PublicChannelData,
}

#[derive(Debug, Clone, serde::Serialize)]
struct PublicChannelData {
    #[serde(rename = "channel")]
    name: String,
}

#[derive(Debug, clap::Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Ip and port of a kafka server
    #[arg(short, long, default_value = "127.0.0.1:9094")]
    kafka_host: String,

    /// Kafka topic name
    #[arg(short, long, default_value = "transactions")]
    topic: String,
}

fn main() -> eyre::Result<()> {
    let args = Args::parse();

    let mut kafka = Producer::from_hosts(vec![args.kafka_host])
        .with_required_acks(kafka::producer::RequiredAcks::All)
        .create()
        .wrap_err("Failed to connect to Kafka")?;

    let mut producer = {
        let (mut socket, response) = tungstenite::connect(BITSTAMP_HOST)
            .wrap_err("Failed to connetct to Bitstamp websocket")?;

        if response.status().is_client_error() || response.status().is_server_error() {
            eprintln!("Server returned {}", response.status());
        }

        subscribe_to_channel(&mut socket, BITSTAMP_EVENT_NAME, BITSTAMP_CHANNEL_NAME)
            .wrap_err("Failed to subscribe to channel")?;

        socket
    };

    loop {
        let message = {
            let message = producer
                .read()
                .wrap_err("Failed to read message from websocket")?;

            if message.is_close() {
                break;
            }

            message
        };

        // println!("Message: {message:?}");

        kafka
            .send(&Record::from_value(&args.topic, message.into_data()))
            .wrap_err("Failed to send message to Kafka")?;
    }

    Ok(())
}

fn subscribe_to_channel<S: Into<String>>(
    socket: &mut WebSocket<MaybeTlsStream<TcpStream>>,
    event: S,
    channel: S,
) -> eyre::Result<()> {
    let message = serde_json::to_string(&Subscribe {
        event: event.into(),
        data: PublicChannelData {
            name: channel.into(),
        },
    })
    .wrap_err("Failed to build message")?;

    socket
        .send(tungstenite::Message::Text(message))
        .wrap_err("Failed to send message")
}
