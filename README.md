## Lab2: Kafka

### Dependencies
- Make
- Docker
- Docker Compose

### Build and run
```
make run
```

### Stop
```
make stop
```