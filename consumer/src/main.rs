use clap::Parser;
use eyre::Context;
use kafka::consumer::{Consumer, FetchOffset, GroupOffsetStorage};

#[derive(Debug, clap::Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Ip and port of a kafka server
    #[arg(short, long, default_value = "127.0.0.1:9094")]
    kafka_host: String,

    /// Kafka topic name
    #[arg(short, long, default_value = "transactions")]
    topic: String,

    /// Consumer group id
    #[arg(short, long, default_value = "0")]
    consumer_group: String,
}

#[derive(Debug, Clone, Default, serde::Deserialize, serde::Serialize)]
struct TransactionData {
    id: u128,
    id_str: String,
    order_type: u64,

    datetime: String,
    microtimestamp: String,

    amount: f64,
    amount_str: String,
    amount_traded: String,
    amount_at_create: String,

    price: f64,
    price_str: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Event {
    Created,
    Deleted,
}

fn parse_transaction(message: &[u8]) -> Option<(Event, TransactionData)> {
    let json = serde_json::from_slice::<serde_json::Value>(message).ok()?;

    let data = &json["data"];

    if *data != serde_json::Value::Object(serde_json::Map::new()) {
        let event = if json["event"] == "order_created" {
            Event::Created
        } else {
            Event::Deleted
        };

        Some((
            event,
            serde_json::from_value::<TransactionData>(data.clone()).unwrap(),
        ))
    } else {
        None
    }
}

fn main() -> eyre::Result<()> {
    let args = Args::parse();

    let mut kafka = Consumer::from_hosts(vec![args.kafka_host])
        .with_topic(args.topic)
        .with_group(args.consumer_group)
        .with_fallback_offset(FetchOffset::Earliest)
        .with_offset_storage(Some(GroupOffsetStorage::Kafka))
        .create()
        .wrap_err("Failed to connect to Kafka")?;

    let mut top_ten = Vec::new();

    loop {
        let messages = kafka.poll().wrap_err("Failed to poll Kafka")?;

        for message_set in messages.iter() {
            for message in message_set.messages() {
                if let Some((event, transaction)) = parse_transaction(message.value) {
                    if event == Event::Deleted {
                        print!("New: {transaction:?}");
                        top_ten.push(transaction);
                        top_ten.sort_unstable_by(|a, b| a.price.total_cmp(&b.price).reverse());

                        if top_ten.len() > 10 {
                            top_ten.pop();
                        }

                        print_result(&top_ten);
                    }
                }
            }
            kafka
                .consume_messageset(message_set)
                .wrap_err("Failed to consume message set")?;
        }

        kafka
            .commit_consumed()
            .wrap_err("Failed to commit consumed message sets")?;
    }
}

/*fn clear_screen() {
    if cfg!(target_os = "windows") {
        std::process::Command::new("cmd")
            .args(["/c", "cls"])
            .spawn()
            .expect("cls command failed to start")
            .wait()
            .expect("failed to wait");
    } else {
        std::process::Command::new("clear")
            .spawn()
            .expect("clear command failed to start")
            .wait()
            .expect("failed to wait");
    };
}*/

fn print_result(result: &[TransactionData]) {
    struct Output {
        id: u128,
        price: f64,
    }

    impl std::fmt::Debug for Output {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(
                f,
                "Transaction with id {}, with price {}",
                self.id, self.price
            )
        }
    }

    println!(
        "\nTop 10:\n{:#?}",
        result
            .into_iter()
            .map(|t| Output {
                id: t.id,
                price: t.price
            })
            .collect::<Vec<_>>()
    )
}
