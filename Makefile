.PHONY: run stop

run:
	docker-compose build
	docker-compose up

stop:
	docker-compose down -v